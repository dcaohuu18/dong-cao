# Bash cheatsheet 
## Basics
* **Ctrl + A**: moves cursor to the beginning of the line
* **Ctrl + E**: moves cursor to the end of the line
* `$ echo <something>`:  returns whatever you type at the shell prompt, similar to  `print()` .
* `$ date`: displays the current time and date.
* `$ clear` or **Ctrl + L**: clears the terminal
* Common shorthand:
	* `.`: (current) working directory
	* `..`: parent of the working directory
		* `../..`: grandparent of the working directory
	* `~`: home directory
* `$ pwd`: prints working directory
* `$ ls`: lists the contents of the working directory
	* `$ ls <particular-directory>` : lists the contents of a particular directory.
	* `$ ls -a`: lists all the hidden objects in a directory
	* `$ ls -l`: prints out a longer and more detailed listing of the contents. 
	* `$ ls <pattern` : lists all the objects matching the given pattern (see **Regular expressions**).
* `$ cd <to-directory>`: jumps to the given directory
* `$ mkdir <new-directory>`: makes a new directory
* `$ mv <things-to-move> <new-location>`: move objects to a given location. It can also be used to rename objects. In that case, the `<new-location>` will be the new name.
* `$ touch <new-file-name>`: creates a new file
* `$ rm <file-name>`: removes a file
	*  `$ rm *`: removes all files in the working directory.
	* `$ rm -r *`: removes all files in the working directory and its children directories (`-r` stands for "recursively").
* `$ rmdir <empty-directory>`: removes an empty directory. By default, a directory cannot be removed unless it has been emptied. 
* `$ cat <file1> <file2> <...>`: reads and outputs the  entire content of the given file(s).
* `$ less <file-name>`: views the content of the given file, one screen at a time. Press:
	* **Spacebar** : to go to the next screen
	* **b**: to go to the previous screen
	* **/**: to search for a specific word
	* **q**: quit
* `$ man <command>`: displays the manual for the given command
	* `$ man bash`: displays the entire manual

## History
* **Up Arrow**  or **Ctrl+P**: goes to the previous command(s) in your history.
* **Down Arrow**  or **Ctrl+N**: goes to the next command(s) in your history.
* **Ctrl+R**: recalls the last command matching the characters you provide. Press this shortcut and start typing to search your bash history for a command.
	* **Ctrl+O**: Run the command you found with Ctrl+R.
	* **Ctrl+G**: Leave the history searching mode without running a command.
* `$ history`: prints your entire bash history to the screen

## Regular Expressions
* `*`: wildcard to match any other character within a given pattern. 
	* Example: `$ ls 5*.txt` lists all `.txt` files whose names start with the number 5.
* `X*`: matches zero or more of `X`
`X+`: matches one or more of `X`
`X?`: matches zero or one of `X`
`X{3, 5}`: matches from 3 to 5 of `X`
* `[0-9]`: matches any digit
`[a-z]`: matches any lowercase letter  
`[A-Z]`: matches any uppercase letter   
`[a-zA-Z]`: matches any letter
* `(<group>)`: declares a group
	* Example: `([a-z]+)@([a-z]+).com` 
* `^`: signifies the beginning of the line
`$`: signifies the end of the line
* `$ egrep '<pattern>' <file1> [file2]`: prints any line from a file that contains a specific pattern of characters.
	* Example: `$ egrep '([a-z]+)12([a-z]+)' doc.txt` prints the lines from `doc.txt` which include the number 12 with at least one character on both sides.
	* `$ egrep -w '<pattern>' *`: only searches for whole words in all the files of the current directory.
	* `$ egrep -i '<pattern>' <file1>`: makes the search case-insensitive.
	* `$ egrep -c '<pattern>' <file1>`: counts the number of matches.
	* `$ egrep -v '<pattern>' <file1>`: excludes the lines that match the given pattern.

## Pipes and Redirection
* `$ command1 | command2`: sends the output of one command as input to another command
	* Example: `$ cat document.txt | wc -l` returns the number of lines in `document.txt`.
* `$ command > file-name`: saves the output of one command to the given file. 
	* Example: `$ cat document.txt | wc -l > lines-count.dat` writes the number of lines of `documents.txt` to `lines-count.dat`.
	**Notice** that the file will be overwritten.
* `$ command < file-name`: sends the given file as input to the command. 
	* Example:  `$ cat < documents.txt` is the same as `$ cat document.txt`.

## Bash Scripting
* Variables:
	```bash
	name="John"
	echo $name
	echo "Hi $name"
	echo "Hey ${name}!"
	
	echo ${name/J/j}    #=> "john" (substitution)
	echo ${name:0:2}    #=> "Jo" (slicing)
	echo ${name::2}     #=> "Jo" (slicing)
	echo ${name::-1}    #=> "Joh" (slicing)
	echo ${name:(-1)}   #=> "n" (slicing from right)
	echo ${name:(-2):1} #=> "h" (slicing from right)
	echo ${food:-Cake}  #=> $food or "Cake"

	emoji="haha"
	echo ${emoji#*h}  #=> "aha" (dropping substr from start of string up to 1st occurrence)
	echo ${emoji##*h} #=> "a" (dropping substr from start of string up to last occurrence)
	echo ${emoji%h*}  #=> "ha" (dropping substr from last occurrence to end of string)
	echo ${var%%a*}   #=> "h" (dropping substr from first occurrence to end of string)
	```
* For loop: `for <...>; do <command1>; [<command2>;] done`
	* `for char in a b c x y z; do echo $char; done` 
	* `for filename in *.dat; do echo $filename; done` 
	* `for i in {1..100}; do echo $RANDOM; done | cat -n > file`: makes a file with 100 random numbers
* You can write and store a script in a file with the `.sh` extension. To make the script executable: `$ chmod +x <script-file-name>`. You can then run it with the syntax: `$ ./<script-file-name>`. The more general syntax to run a script is: `$ /path/to/script.sh`.

## Job Control
* On the shell, multiple processes can be joined together into a 		pipeline, where each process's output becomes the next process's input. A pipeline created this way is a single job even though it contains multiple processes. So think of a job as a process group. Notice that a job may contain only one single process. (credit to [Brian Bi](https://www.quora.com/What-is-the-difference-between-jobs-and-processes-on-Unix))
* To list all the current active jobs: `$ jobs [-l]`.
To list the currently running processes: `$ ps`.
* To run a command in the background, add the `&` symbol: `$ <command> &`
    To keep a process running after the shell exits: `$ nohup <command> &`
    Both the commands above print out the shell job ID (in brackets) and the process ID (PID), e.g. `[1] 25177`.
 * To send foreground jobs to the background: `$ bg %jobID_1 [%jobID_2]`. You can replace the job ID with the command string or use the PID directly (the `%` symbol is not needed): `$ bg PID_1` .
 To `fg` command is used similarly to bring background jobs to the foreground. 
 
## Module
* `$ module list`: lists currently loaded modules
* `$ module avail`: lists available packages
* `$ module load <module-file>`: loads module or specifies which dependencies have not been loaded.
	* E.g. `$ module load python/3.6`
* `$ module unload <module-file>`: unloads the specified module from the environment.
* `$ module purge`: unloads all loaded modules.

## Miscellaneous 
* `$ tar -czvf archive-name.tar.gz /path/to/dir-or-file`: compresses an entire directory or a single file.
`$ tar -xzvf archive-name.tar.gz`: extracts an archive.
* `$ diff <file1> <file2>`: finds the differences between the two files.