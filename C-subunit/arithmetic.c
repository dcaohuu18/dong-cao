#include <stdio.h>
#include <stdlib.h>
#include <string.h>


float take_average(unsigned long int sum, unsigned long int	count) {
    return (float)sum / (float)count;
}


int main(int argc, char *argv[]) {
    FILE *inputFileHandle = NULL; 
    char inputFileName[128]; 
    unsigned long int value = 0; 
    unsigned long int sum = 0;
    unsigned long int count = 0;
    float average = 0.0; 

    if (argc < 3) {
        printf("usage: fopen -i <infile>\n");
        exit(1);
    }

    if (strcmp(argv[1], "-i") == 0) {
        strcpy(inputFileName, argv[2]);
    }
    else {
        printf("usage: fopen -i <infile>\n");
        exit(1);
    }

    if ((inputFileHandle = fopen(inputFileName, "r")) == NULL) {
        fprintf(stderr, "error opening file: %s\n", inputFileName); 
        exit(1);
    }

    while (fscanf(inputFileHandle, "%d", &value) != EOF) {
        count++;
        sum = sum + value; 
    }

    fclose(inputFileHandle); 	

    average = take_average(sum, count);

    fprintf(stdout, "file = %s, count = %d, sum = %d, average =%8.2f\n", 
            inputFileName, count, sum, average); 

    return(0); 
}
