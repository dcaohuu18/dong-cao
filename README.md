*Dong Cao, Senior, Computer Science*

# CS 266 Computing Skills 
A module-based course provides students with practice using the computing toolchains and technical skills they will need to use throughout their courses and careers. Students will complete two modules during the course: a fundamental computing tools module and one additional module of their choice. For each module, each student will complete a project that will teach them and allow them to demonstrate their proficiency with it. The Moodle page of the course can be found [here](https://moodle.earlham.edu/course/view.php?id=4903).

## Fundamental Computing Skills 
* Command line utilities and Bash
* Version control with Git
* Markdown for composing readable text
* Introduction to regular expressions
* C programming language. Here is a simple `hello-world` program:
```C
#include <stdio.h>

int main(void) {
  printf("Hello World\n");
  return 0;
}
```
* Building and installing software
* Batch computing and scheduling
